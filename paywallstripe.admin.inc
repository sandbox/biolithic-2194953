<?php
/**
 * Configuration Form for Admin to set the secret and publishable keys.
 *
 * @return array
 *   Array of form values to be saved.
 */
function paywallstripe_admin_settings()
{
    $form = array(
        '#type' => 'fieldset',
        '#title' => t('My Module Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE
    );
    
    $form['paywallstripe_readme'] = array(
        '#type' => 'markup',
        '#title' => t('Read me instructions'),
        '#markup' => '<p>' . l(t('Read me instructions'), 'admin/config/services/paywallstripe/readme') . '</p>'
    );
    
    $form['paywallstripe_whatits'] = array(
        '#type' => 'markup',
        '#title' => t('What is this module?'),
        '#markup' => '<p> PaywallStripe lets you define content types as premium and then sell a membership for them using Stripe.com to process the payments.  Drupal Commerce is recommended for this, but if that suite is too large for your needs, this is a simple payment solution to sell access to nodes.</p>'
    );
    
    $form['paywallstripe_warning'] = array(
        '#type' => 'markup',
        '#title' => t('What happens when my customer --buys now--?'),
        '#markup' => '<p> This module does not save ANYTHING about the credit cards or payment to this website.  The user may type it into the form on this website which gets sent immediately only to Stripe encrypted and you will only get a pass/fail message back.  Do not save anything about the payment for re-occuring billing, record keeping, or anything like that to this site.  You may view any details about each transaction at your Stripe.com account page. <br>&nbsp;<br>This module requires Javascript to be enabled for your site through out the process to work properly.  Your customers cannot buy products through your website using this module if they have disabled Javascript in their browser. </p>'
    );
    
    $form['paywallstripe_wherefrom'] = array(
        '#type' => 'markup',
        '#title' => t('Where to get the TEST information for this page from?'),
        '#markup' => '<p>' . l(t('Sign up for a Stripe test account:'), 'https://stripe.com') . '</p>'
    );
    
    $form['paywallstripe_wherefrom2'] = array(
        '#type' => 'markup',
        '#title' => t('Where to get the LIVE information for this page from?'),
        '#markup' => '<p>' . l(t('Sign up for a Stripe live account:'), 'https://stripe.com') . '</p>'
    );
    
    $form['paywallstripe_publishable_testkey'] = array(
        '#type' => 'textfield',
        '#title' => t('Stripe test public API Key'),
        '#default_value' => variable_get('paywallstripe_publishable_testkey', ""),
        '#description' => t('Stripe test public API Key'),
        '#required' => TRUE
    );
    
    $form['paywallstripe_publishable_livekey'] = array(
        '#type' => 'textfield',
        '#title' => t('Stripe live public API Key'),
        '#default_value' => variable_get('paywallstripe_publishable_livekey', ""),
        '#description' => t('Stripe live public API Key'),
        '#required' => TRUE
    );
    
    $form['paywallstripe_secret_testkey'] = array(
        '#type' => 'textfield',
        '#title' => t('Stripe test secret API Key'),
        '#default_value' => variable_get('paywallstripe_secret_testkey', ""),
        '#description' => t('Stripe test secret API Key'),
        '#required' => TRUE
    );
    
    $form['paywallstripe_secret_livekey'] = array(
        '#type' => 'textfield',
        '#title' => t('Stripe live secret API Key'),
        '#default_value' => variable_get('paywallstripe_secret_livekey', ""),
        '#description' => t('Stripe live secret API Key'),
        '#required' => TRUE
    );
    
    $form['paywallstripe_price'] = array(
        '#type' => 'textfield',
        '#title' => t('Price in USA dollars of membership'),
        '#default_value' => variable_get('paywallstripe_price', ""),
        '#description' => t('Price in USA dollars of membership'),
        '#required' => TRUE
    );
    
    $form['paywallstripe_gonelive'] = array(
        '#type' => 'checkbox',
        '#title' => t('Live?'),
        '#default_value' => variable_get('paywallstripe_gonelive', ""),
        '#description' => t('Check this box to use your live key.  Uncheck to enter demo mode and use your test key')
    );
    
    $form['paywallstripe_sslmessage'] = array(
        '#type' => 'checkbox',
        '#title' => t('SSL error message?'),
        '#default_value' => variable_get('paywallstripe_sslmessage', ""),
        '#description' => t('This is not recommended, but you can get away with using this module on non-SSL/http servers.  To show an error message at the top of this page while you dont have an HTTPS secure connection, so that site admins know, check this box.')
    );
    
    $form['paywallstripe_sendemail'] = array(
        '#type' => 'checkbox',
        '#title' => t('Send emails to admin and customer for each successful transaction?'),
        '#default_value' => variable_get('paywallstripe_sendemail', ""),
        '#description' => t('Check this box to send emails.  Uncheck to not send emails.')
    );
    
    foreach (array_keys(node_type_get_names()) as $term) {
        $form["paywallstripe_$term"] = array(
            '#type' => 'checkbox',
            '#title' => t("Do you want to charge for $term ?"),
            '#default_value' => variable_get("paywallstripe_$term", ""),
            '#description' => t("Uncheck to allow all users to view nodes of content type $term .  Check to limit access to paying members.")
        );
    }
    
    return system_settings_form($form);
}